# parse-conf package

This library is used to parse the full configuration object used in the accessibility testing of the [Torgersen Sage Wordpress Theme](https://git.it.vt.edu/rfentres/torgersen).  The original configuration object contains a full listing of test suites specifying which tests to run on which pages when specific files change.  The configuration can also contain templates that specify configuration information shared by multiple page scans.  parse-conf expands these templates and filters the test suites to only the specific files or web pages of interest.  This filtered set of suites is then used by the accessibility testing functions of the WordPress theme.

## API
_**Note:** Some of these properties and methods don't need to be public, but I'm still getting used to TypeScript and was having trouble running my Mocha tests without doing so.  Asterisk placed beside properties which may be made private in the future._
### ParseConf

#### Public Properties

##### fullConf *

The full configuration data passed into the constructor

##### tempConf *

A copy of the original full configuration data used for processing parsed results

##### paths *

The paths passed into createSuites and used to filter the configuration data

#### Public Methods

##### constructor(fullConf: IFullConf)
Creates an object allowing us to parse a JSON data containing configuration info for the scans we want to run
* @constructs Parse
* @param {IFullConf} fullConf - The configuration data to parse

##### createSuites(paths: string[])
Returns object containing a list of all the test suites to be run
* @readonly
* @returns {ISuite[]} - Object containing a list of all the test suites to be run

##### getPageSuite(path: string): IConf[]
Populate fullSuites with an array of suites based on the paths parameter and JSON file referenced in constructor
* @param {string} path - Path to page (relative to root of the site) to be checked for matches

##### expandTemplates(replaceOptions: boolean = true)
Expands template refs in suites into conf objects, optionally converting options into axe runOnly type rule values
* @param {boolean=true} replaceOptions - Whether to convert options into [axe runOnly type](https://github.com/dequelabs/axe-core/blob/master/doc/API.md#options-parameter) rule id [values](https://github.com/dequelabs/axe-core/blob/master/doc/rule-descriptions.md)

##### expandSuite(suite: ISuite) *
Expands references to templates in a particular suite to conf objects
* @param {ISuite} suite - Suite to expand

##### expandPage(page: IPage) *
In given page takes references to any templates and merges the conf objects they contain into the conf for the page
* @param {IPage} page - Info about the page to be scanned, provided by reference to template and/or with conf object


### Scan

#### Public Methods

##### constructor(public conf: IConf = {})
Helper that provides title & description of scan if not in conf, and generates runOnly of type "rule" from [options](https://github.com/dequelabs/axe-core/blob/master/doc/API.md#options-parameter)
* @constructs Scan
* @param {IConf} conf - The original configuration information to be transformed

#### Public Properties

##### conf *

The original configuration information to be transformed

##### rules
Parsed list of [rules id](https://github.com/dequelabs/axe-core/blob/master/doc/rule-descriptions.md) values corresponding to the rules to be run for a particular `page` object.  Created from [options](https://github.com/dequelabs/axe-core/blob/master/doc/API.md#options-parameter) in `conf` of `page` object.

##### title
The title to be used when displaying the scan results.  If not provided in `meta.title` of `conf` object passed in constructor, it is generated from the [context](https://github.com/dequelabs/axe-core/blob/master/doc/API.md#context-parameter) object.

##### description
The description to be used when displaying the scan results.  If not provided in `meta.description` of `conf` object passed in constructor, it is generated from the [options](https://github.com/dequelabs/axe-core/blob/master/doc/API.md#options-parameter) object.
