import { IConf, IFullConf, IPage, IParseConf, ISuite } from "./Model";
export declare class Parse implements IParseConf {
    private static expressionMatches(match, path);
    fullConf: IFullConf;
    tempConf: IFullConf;
    paths: string[];
    private suites;
    private replaceOptions;
    constructor(fullConf: IFullConf);
    readonly fullSuite: ISuite[];
    createSuites(paths: string[]): void;
    getPageSuite(path: string): IConf[];
    expandTemplates(replaceOptions?: boolean): void;
    expandSuite(suite: ISuite): void;
    expandPage(page: IPage): void;
    private mergeConfs(page);
    private stackConfs(template, confs);
    private process(path);
    private processFiles(path);
    private processExpressions(path);
}
