import * as axe from "axe-core";
export declare type Operation = "+" | "-";
export declare type Selector = "tags" | "rules";
export interface IRules {
    [key: string]: axe.Rule;
}
export interface IncludeExclude {
    include?: string[];
    exclude?: string[];
}
export interface IRunOnly {
    type: axe.RunOnlyType;
    value?: IncludeExclude;
    values?: string[];
}
export interface IConf {
    meta?: IMeta;
    context?: IContext;
    options?: IOptions;
}
export interface IMeta {
    title?: string;
    description?: string;
}
export interface IScan {
    title?: string;
    description?: string;
    rules?: any[];
}
export interface IContext extends axe.ElementContext {
    node?: Object;
    selector?: string;
    include?: any[];
    exclude?: any[];
}
export interface IOptions {
    runOnly?: IRunOnly;
    rules?: Object;
}
export interface ITemplate {
    match: string;
    template?: string;
    conf?: IConf;
}
export interface IPage extends ITemplate {
    match: string;
    template?: string;
    conf?: IConf;
}
export interface ISuite {
    match?: string;
    pages?: IPage[];
}
export interface IFile extends ISuite {
    match: string;
    pages?: IPage[];
}
export interface IExpression extends ISuite {
    match: string;
    once?: boolean;
    pages?: IPage[];
}
export interface IFullConf {
    files?: IFile[];
    expressions?: IExpression[];
    templates?: ITemplate[];
}
export interface IParseConf {
    fullConf: IFullConf;
    readonly fullSuite: ISuite[];
}
