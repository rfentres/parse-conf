"use strict";
var _ = require("lodash");
var Scan_1 = require("./Scan");
var Parse = (function () {
    function Parse(fullConf) {
        this.fullConf = {};
        this.tempConf = {};
        this.paths = [];
        this.suites = [];
        this.replaceOptions = true;
        this.fullConf = fullConf;
    }
    Parse.expressionMatches = function (match, path) {
        var literalStr = match.substr(0, match.lastIndexOf("/"));
        var pattern = literalStr.replace(/^\/|\/$/g, "");
        var flag = /[^/]*$/.exec(match)[0];
        var re = new RegExp(pattern, flag);
        return re.test(path);
    };
    Object.defineProperty(Parse.prototype, "fullSuite", {
        get: function () {
            return this.suites;
        },
        enumerable: true,
        configurable: true
    });
    Parse.prototype.createSuites = function (paths) {
        this.paths = paths.slice();
        this.tempConf = _.cloneDeep(this.fullConf);
        this.suites = [];
        for (var i = 0; i < this.paths.length; i++) {
            this.process(this.paths[i]);
        }
    };
    Parse.prototype.getPageSuite = function (path) {
        this.tempConf = _.cloneDeep(this.fullConf);
        var processed = [];
        var suiteTypes = ["files", "expressions"];
        for (var my = 0; my < suiteTypes.length; my++) {
            var suites = this.tempConf[suiteTypes[my]];
            for (var i = 0; i < suites.length; i++) {
                var suite_1 = suites[i];
                for (var n = 0; n < suite_1.pages.length; n++) {
                    if (suite_1.pages[n].match === path) {
                        var page = suite_1.pages[n];
                        this.mergeConfs(page);
                        var scan = new Scan_1.Scan(page.conf);
                        _.merge(page.conf, {
                            meta: {
                                description: scan.description,
                                title: "File/RegExp Match: " + suite_1.match,
                            },
                        });
                        processed.push(page.conf);
                        suite_1.pages.splice(n, 1);
                        n--;
                    }
                }
            }
        }
        return _.cloneDeep(processed);
    };
    Parse.prototype.expandTemplates = function (replaceOptions) {
        if (replaceOptions === void 0) { replaceOptions = true; }
        this.replaceOptions = replaceOptions;
        for (var i = 0; i < this.suites.length; i++) {
            this.expandSuite(this.suites[i]);
        }
    };
    Parse.prototype.expandSuite = function (suite) {
        for (var i = 0; i < suite.pages.length; i++) {
            this.expandPage(suite.pages[i]);
        }
    };
    Parse.prototype.expandPage = function (page) {
        this.mergeConfs(page);
        var scan = new Scan_1.Scan(page.conf);
        _.merge(page.conf, {
            meta: {
                description: scan.description,
                title: scan.title,
            },
        });
        if (this.replaceOptions) {
            page.conf.options = {
                runOnly: {
                    type: "rule",
                    values: scan.rules,
                },
            };
        }
    };
    Parse.prototype.mergeConfs = function (page) {
        var merged = {};
        var confs = this.stackConfs(page.template, []);
        confs.push(page.conf);
        _.merge.apply(_, [merged].concat(confs));
        page.conf = merged;
    };
    Parse.prototype.stackConfs = function (template, confs) {
        var parent = _.find(this.tempConf.templates, { match: template });
        if (typeof parent !== "undefined") {
            confs.unshift(parent.conf);
            if (typeof parent.template !== "undefined") {
                this.stackConfs(parent.template, confs);
            }
        }
        return confs;
    };
    Parse.prototype.process = function (path) {
        this.processFiles(path);
        this.processExpressions(path);
    };
    Parse.prototype.processFiles = function (path) {
        for (var i = 0; i < this.tempConf.files.length; i++) {
            if (this.tempConf.files[i].match === path) {
                this.suites.push(this.tempConf.files[i]);
                this.tempConf.files.splice(i, 1);
                i--;
            }
        }
    };
    Parse.prototype.processExpressions = function (path) {
        for (var i = 0; i < this.tempConf.expressions.length; i++) {
            if (Parse.expressionMatches(this.tempConf.expressions[i].match, path)) {
                var expressionMap = { match: "" };
                _.assign(expressionMap, this.tempConf.expressions[i]);
                expressionMap.match = path;
                if (this.tempConf.expressions[i].once === true) {
                    this.tempConf.expressions.splice(i, 1);
                    i--;
                    delete expressionMap.once;
                }
                this.suites.push(expressionMap);
            }
        }
    };
    return Parse;
}());
exports.Parse = Parse;
