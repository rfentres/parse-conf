import { IConf, IScan } from "./../Model";
export declare class Scan implements IScan {
    conf: IConf;
    rules: any[];
    private meta;
    private context;
    private options;
    private myDesc;
    private includedTags;
    private includedRules;
    private excludedRules;
    private excludedTags;
    constructor(conf?: IConf);
    private addToDesc(addOrSubtract, type, values);
    private setRules(rules?);
    private prepRulesAndTags();
    readonly description: string;
    readonly title: string;
}
