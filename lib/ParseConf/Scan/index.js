"use strict";
var axe = require("axe-core");
var _ = require("lodash");
var Scan = (function () {
    function Scan(conf) {
        if (conf === void 0) { conf = {}; }
        this.conf = conf;
        this.rules = [];
        this.myDesc = [];
        this.includedTags = [];
        this.includedRules = [];
        this.excludedRules = [];
        this.excludedTags = [];
        this.context = conf.context || {};
        this.options = conf.options || {};
        this.meta = conf.meta || {};
        this.myDesc = [];
        this.includedTags = [];
        this.includedRules = [];
        this.excludedRules = [];
        this.excludedTags = [];
        this.rules = [];
        this.prepRulesAndTags();
    }
    Scan.prototype.addToDesc = function (addOrSubtract, type, values) {
        this.myDesc.push(addOrSubtract + type + JSON.stringify(values));
    };
    ;
    Scan.prototype.setRules = function (rules) {
        var _this = this;
        if (rules === void 0) { rules = {}; }
        Object.keys(rules).forEach(function (key) {
            if (rules[key].enabled === true) {
                _this.includedRules = _.union([key], _this.includedRules);
                _this.rules = _.union([key], _this.rules);
            }
            else {
                _this.excludedRules = _.union([key], _this.excludedRules);
                _this.rules = _.difference(_this.rules, [key]);
            }
        });
    };
    ;
    Scan.prototype.prepRulesAndTags = function () {
        if (typeof this.options.runOnly !== "undefined") {
            var runOnly = this.options.runOnly;
            if (runOnly.type === "tag") {
                if ((Array.isArray(runOnly.values)) && (runOnly.values.length > 0)) {
                    this.includedTags = runOnly.values;
                    this.rules = _.map(axe.getRules(this.includedTags), "ruleId");
                }
            }
            else if (runOnly.type === "rule") {
                if ((Array.isArray(runOnly.values)) && (runOnly.values.length > 0)) {
                    this.includedRules = runOnly.values;
                    this.rules = this.includedRules;
                }
            }
            else if (runOnly.type === "tags") {
                if (typeof runOnly.value !== "undefined") {
                    if ((Array.isArray(runOnly.value.include)) && (runOnly.value.include.length > 0)) {
                        this.includedTags = runOnly.value.include;
                        this.rules = _.map(axe.getRules(this.includedTags), "ruleId");
                    }
                    if ((Array.isArray(runOnly.value.exclude)) && (runOnly.value.exclude.length > 0)) {
                        this.excludedTags = runOnly.value.exclude;
                        this.rules = _.difference(this.rules, _.map(axe.getRules(this.excludedTags), "ruleId"));
                    }
                }
            }
            else {
                this.rules = _.map(axe.getRules(), "ruleId");
            }
        }
        else {
            this.rules = _.map(axe.getRules(), "ruleId");
        }
        if (typeof this.options.rules !== "undefined") {
            this.setRules(this.options.rules);
        }
    };
    Object.defineProperty(Scan.prototype, "description", {
        get: function () {
            if (typeof this.meta.description === "undefined") {
                if (this.includedTags.length > 0) {
                    this.addToDesc("+", "tags", this.includedTags);
                }
                if (this.excludedTags.length > 0) {
                    this.addToDesc("-", "tags", this.excludedTags);
                }
                if (this.includedRules.length > 0) {
                    this.addToDesc("+", "rules", this.includedRules);
                }
                if (this.excludedRules.length > 0) {
                    this.addToDesc("-", "rules", this.excludedRules);
                }
                return this.myDesc.join(", ") || "No rules or tags specified";
            }
            else {
                return this.meta.description;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Scan.prototype, "title", {
        get: function () {
            if (typeof this.meta.title === "undefined") {
                var title = (typeof this.context.include === "undefined") ?
                    "+document" :
                    "+" + JSON.stringify(this.context.include);
                if (typeof this.context.exclude !== "undefined") {
                    title += (this.context.exclude.length > 0) ? ", -" + JSON.stringify(this.context.exclude) : "";
                }
                return title;
            }
            else {
                return this.meta.title;
            }
        },
        enumerable: true,
        configurable: true
    });
    return Scan;
}());
exports.Scan = Scan;
