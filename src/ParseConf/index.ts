import * as _ from "lodash";
import {
  IConf,
  IExpression,
  IFullConf,
  IPage,
  IParseConf,
  ISuite,
} from "./Model";
import { Scan } from "./Scan";

/**
 * Creates an object allowing us to parse a JSON file containing configuration info for the scans we want to run
 * @class
 */
export class Parse implements IParseConf {

  /**
   * Indicates whether a regular expression literal converted to a JSON-safe string matches the path to a file
   * @param {string} match - A regular expression literal converted to a JSON-safe string
   * @param {string} path - The relative path from the root of the project to the file
   * @returns {boolean} - Whether the expression matches the path
   */
  private static expressionMatches(match: string, path: string): boolean {
    let literalStr = match.substr(0, match.lastIndexOf("/")); // everything except flag
    let pattern = literalStr.replace(/^\/|\/$/g, ""); // strip first and last slash
    let flag = /[^/]*$/.exec(match)[0];
    let re = new RegExp(pattern, flag);
    return re.test(path);
  }

  public fullConf: IFullConf = {};
  public tempConf: IFullConf = {};
  public paths: string[] = [];

  /**
   * Where generated suites are stored
   * @private {ISuite[]}
   */
  private suites: ISuite[] = [];

  private replaceOptions: boolean = true;

  /**
   * Creates an object allowing us to parse a JSON data containing configuration info for the scans we want to run
   * @constructs Parse
   * @param {IFullConf} fullConf - The configuration data to parse
   */
  constructor(fullConf: IFullConf) {
    this.fullConf = fullConf;
  }

  /**
   * Returns object containing a list of all the test suites to be run
   * @readonly
   * @returns {ISuite[]} - Object containing a list of all the test suites to be run
   */
  get fullSuite(): ISuite[] {
    return this.suites;
  }

  /**
   * Populate fullSuites with an array of suites based on the paths parameter and JSON data referenced in constructor
   * @param {string[]} paths - A list of paths (relative to root of project) to be checked for matches
   */
  public createSuites(paths: string[]) {

    this.paths = paths.slice();
    this.tempConf = _.cloneDeep(this.fullConf);
    this.suites = [];

    for (let i = 0; i < this.paths.length; i++) {
      this.process(this.paths[i]);
    }

  }

  /**
   * Populate fullSuites with an array of suites based on the paths parameter and JSON file referenced in constructor
   * @param {string} path - Path to page (relative to root of the site) to be checked for matches
   */
  public getPageSuite(path: string): IConf[] {
    this.tempConf = _.cloneDeep(this.fullConf);
    let processed: IConf[] = [];

    let suiteTypes: Array<"files" | "expressions"> = ["files", "expressions"];

    for (let my = 0; my < suiteTypes.length; my++) {
      let suites = this.tempConf[suiteTypes[my]];
      for (let i = 0; i < suites.length; i++) {
        let suite = suites[i];
        for (let n = 0; n < suite.pages.length; n++) {
          if (suite.pages[n].match === path) {

            let page: IPage = suite.pages[n];

            this.mergeConfs(page);

            let scan = new Scan(page.conf);

            _.merge(page.conf, {
              meta: {
                description: scan.description,
                title: "File/RegExp Match: " + suite.match,
              },
            });
            processed.push(page.conf);
            suite.pages.splice(n, 1);
            n--;
          }
        }
      }
    }
    return _.cloneDeep(processed);
  }

  /**
   * Expands template refs in suites into conf objects, optionally converting options into axe runOnly type rule values
   * @param {boolean=true} replaceOptions - Whether to convert options into axe runOnly type rule values
   */
  public expandTemplates(replaceOptions: boolean = true) {
    this.replaceOptions = replaceOptions;
    for (let i = 0; i < this.suites.length; i++) {
      this.expandSuite(this.suites[i]);
    }
  }

  /**
   * Expands references to templates in a particular suite to conf objects
   * @param {ISuite} suite - Suite to expand
   */
  public expandSuite(suite: ISuite) {
    for (let i = 0; i < suite.pages.length; i++) {
      this.expandPage(suite.pages[i]);
    }
  }

  /**
   * In given page takes references to any templates and merges the conf objects they contain into the conf for the page
   * @param {IPage} page - Info about the page to be scanned, provided by reference to template and/or with conf object
   */
  public expandPage(page: IPage) {
    this.mergeConfs(page);
    let scan = new Scan(page.conf);
    _.merge(page.conf, {
      meta: {
        description: scan.description,
        title: scan.title,
      },
    });

    if (this.replaceOptions) {
      page.conf.options = {
        runOnly: {
          type: "rule",
          values: scan.rules,
        },
      };
    }
  }

  private mergeConfs(page: IPage) {
    let merged = {};
    let confs: IConf[] = this.stackConfs(page.template, []);

    confs.push(page.conf);
    _.merge(merged, ...confs);

    page.conf = merged;
  }

  /**
   * Recursively populate array with conf objects from matching templates
   * @param {string} template - The match property of the template you want the conf object from
   * @param {IConf[]} confs - The array to stack (actually unshift, technically) the conf object onto
   * @returns {IConf[]} - An array of conf objects from macthing templates
   */
  private stackConfs(template: string, confs: IConf[]): IConf[] {
    let parent = _.find(this.tempConf.templates, {match: template});
    if (typeof parent !== "undefined") {
      confs.unshift(parent.conf);
      if (typeof parent.template !== "undefined") {
        this.stackConfs(parent.template, confs);
      }
    }
    return confs;
  }

  /**
   * Process file and expression objects in the conf file for matches to create suites with pages containing scans
   * @param path
   */
  private process(path: string) {
    this.processFiles(path);
    this.processExpressions(path);
  }

  /**
   * For each file suite in full configuration, add suite to list, if path provided matches file name
   * @param {string} path - Path, relative to root of project, of file you want to find a match for in match property
   */
  private processFiles(path: string) {
    for (let i = 0; i < this.tempConf.files.length; i++) {
      if (this.tempConf.files[i].match === path) {
        this.suites.push(this.tempConf.files[i]);
        this.tempConf.files.splice(i, 1);
        i--;
      }
    }
  }

  /**
   * For each expression suite in full configuration, add suite to list, if path provided is a regular expression match
   * @param {string} path - Path, relative to root of project, of file you want to find a match for in match property
   */
  private processExpressions(path: string) {
    for (let i = 0; i < this.tempConf.expressions.length; i++) {
      if (Parse.expressionMatches(this.tempConf.expressions[i].match, path)) {

        let expressionMap: IExpression = {match: ""};
        _.assign(expressionMap, this.tempConf.expressions[i]);

        expressionMap.match = path;

        if (this.tempConf.expressions[i].once === true) {
          this.tempConf.expressions.splice(i, 1);
          i--;
          delete expressionMap.once;
        }

        this.suites.push(expressionMap);
      }
    }
  }
}
