import * as axe from "axe-core";
import * as _ from "lodash";
import {
  IConf,
  IContext,
  IMeta,
  IOptions,
  IRules,
  IRunOnly,
  IScan,
  Operation,
  Selector,
} from "./../Model";

export class Scan implements IScan {

  public rules: any[] = [];

  private meta: IMeta;
  private context: IContext;
  private options: IOptions;

  private myDesc: string[] = [];
  private includedTags: any[] = [];
  private includedRules: any[] = [];
  private excludedRules: any[] = [];
  private excludedTags: any[] = [];

  /**
   * Helper that provides title & description of scan if not in conf, and generates runOnly of type "rule" from options
   * @constructs Scan
   * @param {IConf} conf - The configuration information to be transformed
   */
  constructor(public conf: IConf = {}) {
    this.context = conf.context || {};
    this.options = conf.options || {};
    this.meta = conf.meta || {};

    this.myDesc = [];
    this.includedTags = [];
    this.includedRules = [];
    this.excludedRules = [];
    this.excludedTags = [];

    this.rules = [];

    this.prepRulesAndTags();

  }

  /**
   * Adds things that will be included and excluded from scan to an array used to generate the description of a scan
   * @param {Operation} addOrSubtract - The operation signifier to prepend to value
   * @param {Selector} type - Indicates whether we are adding rules or tags
   * @param {any[]} values - List of tags or rules to add
   */
  private addToDesc(addOrSubtract: Operation, type: Selector, values: any[]) {
    this.myDesc.push(addOrSubtract + type + JSON.stringify(values));
  };

  /**
   * Adds rules in options.rules to list of rules used when rewriting options and in generating description if needed
   * @param {IRules} rules - Rules to be added or removed, based on axe.Rules
   */
  private setRules(rules: IRules = {}) {
    Object.keys(rules).forEach((key: string) => {
      if (rules[key].enabled === true) {
        this.includedRules = _.union([key], this.includedRules);
        this.rules = _.union([key], this.rules);
      } else {
        this.excludedRules = _.union([key], this.excludedRules);
        this.rules = _.difference(this.rules, [key]);
      }
    });
  };

  /**
   * Sets data structures needed to provide meaningful title, description, and rules properties
   */
  private prepRulesAndTags() {

    if (typeof this.options.runOnly !== "undefined") {

      let runOnly: IRunOnly = this.options.runOnly;

      if (runOnly.type === "tag") {
        if ((Array.isArray(runOnly.values)) && (runOnly.values.length > 0)) {
          this.includedTags = runOnly.values;
          this.rules = _.map(axe.getRules(this.includedTags), "ruleId");
        }
      } else if (runOnly.type === "rule") {
        if ((Array.isArray(runOnly.values)) && (runOnly.values.length > 0)) {
          this.includedRules = runOnly.values;
          this.rules = this.includedRules;
        }
      } else if (runOnly.type === "tags") {
        if (typeof runOnly.value !== "undefined") {
          if ((Array.isArray(runOnly.value.include)) && (runOnly.value.include.length > 0)) {
            this.includedTags = runOnly.value.include;
            this.rules = _.map(axe.getRules(this.includedTags), "ruleId");
          }
          if ((Array.isArray(runOnly.value.exclude)) && (runOnly.value.exclude.length > 0)) {
            this.excludedTags = runOnly.value.exclude;
            this.rules = _.difference(this.rules, <string[]> _.map(axe.getRules(this.excludedTags), "ruleId"));
          }
        }
      } else {
        this.rules = _.map(axe.getRules(), "ruleId");
      }
    } else {
      this.rules = _.map(axe.getRules(), "ruleId");
    }

    if (typeof this.options.rules !== "undefined") {
      this.setRules(<IRules> this.options.rules);
    }
  }

  /**
   * The description of scan, generated from rules & tags provided in options, if meta.description not in conf already
   * @returns {string}
   */
  get description(): string {
    if (typeof this.meta.description === "undefined") {

      if (this.includedTags.length > 0) {
        this.addToDesc("+", "tags", this.includedTags);
      }
      if (this.excludedTags.length > 0) {
        this.addToDesc("-", "tags", this.excludedTags);
      }
      if (this.includedRules.length > 0) {
        this.addToDesc("+", "rules", this.includedRules);
      }
      if (this.excludedRules.length > 0) {
        this.addToDesc("-", "rules", this.excludedRules);
      }

      return this.myDesc.join(", ") || "No rules or tags specified";

    } else {
      return this.meta.description;
    }
  }

  /**
   * The title of the scan, generated from HTML selectors provided in context, if meta.title not in conf already
   * @returns {string}
   */
  get title(): string {
    if (typeof this.meta.title === "undefined") {
      let title = (typeof this.context.include === "undefined") ?
        "+document" :
        "+" + JSON.stringify(this.context.include);
      if (typeof this.context.exclude !== "undefined") {
        title += (this.context.exclude.length > 0) ? ", -" + JSON.stringify(this.context.exclude) : "";
      }
      return title;
    } else {
      return this.meta.title;
    }
  }
}
