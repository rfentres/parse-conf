import * as axe from "axe-core";

export type Operation = "+" | "-";
export type Selector = "tags" | "rules";

/**
 * @typedef {Object} IRules
 */
export interface IRules {
  [key: string]: axe.Rule;
}

/**
 * @typedef IncludeExclude
 * @property {string[]} include - Selectors for elements to include in scan
 * @property {string[]} exclude - Selectors for elements to exclude from scan
 */
export interface IncludeExclude {
  include?: string[];
  exclude?: string[];
}

// Tried to implement axe.RunOnly, but had to change values property to string, since values can be rules not just tags
/**
 * @typedef IRunOnly
 * @property {axe.RunOnlyType} type - tag, rule, or tags
 * @property {IncludeExclude} value - Selectors for elements to include or exclude in scan, only used if type is tags
 * @property {string} values - Used if type is tag or rule and contains either tag name or rule name, respectively
 */
export interface IRunOnly {
  type: axe.RunOnlyType;
  value?: IncludeExclude;
  values?: string[];
}

/**
 * @typedef IConf
 * @property {IMeta} plugin - Specifies title and description of the scan to be run
 * @property {IContext} context - Specifies elements to include or exclude in scan
 * @property {IOptions} options - Specifies which rules to run
 */
export interface IConf {
  meta?: IMeta;
  context?: IContext;
  options?: IOptions;
}

/**
 * @typedef IMeta
 * @property {string} title - The title to be used when displaying results of scan
 * @property {string} description -  The description to be used when displaying results of scan
 */
export interface IMeta {
  title?: string;
  description?: string;
}

/**
 * @typedef IScan
 * @property {string} title - The title to be used when displaying results of scan
 * @property {string} description -  The description to be used when displaying results of scan
 * @property {any[]} rules -  The rules that will be run for the scan
 */
export interface IScan {
  title?: string;
  description?: string;
  rules?: any[];
}

/**
 * @typedef IContext
 * @property {Object} node -
 * @property {string} selector - Selectors for elements to include or exclude in scan, only used if type is tags
 * @property {any[]} include - Selectors for elements to include in scan
 * @property {any[]} exclude - Selectors for elements to exclude from scan
 */
export interface IContext extends axe.ElementContext {
  node?: Object;
  selector?: string;
  include?: any[];
  exclude?: any[];
}

/**
 * @typedef IOptions
 * @property {IRunOnly} runOnly - Rules to run--if provided, then overrides defaults & runs only these and ones in rules
 * @property {Object} rules - Contains rules essentially of type axe.Rule, but I couldn't get that to work like I wanted
 */
export interface IOptions {
  runOnly?: IRunOnly;
  rules?: Object; // IRules;
}

/**
 * @typedef ITemplate
 * @property {string} match - How the template is identified for matching purposes
 * @property {string} template - The match property of another template object, which current one is merged with
 * @property {IConf} conf - Provides configuration information used for a scan
 */
export interface ITemplate {
  match: string;
  template?: string;
  conf?: IConf;
}
/**
 * @typedef IPage
 * @property {string} match - The path, relative to root of site, of a web page to be scanned
 * @property {string} template - The match property of another template object, which current one is merged with
 * @property {IConf} conf - Provides configuration information used for a scan
 */
export interface IPage extends ITemplate {
  match: string;
  template?: string;
  conf?: IConf;
}

/**
 * @typedef ISuite
 * @property {string} match - How suite is identified and matched
 * @property {IPage[]} pages - List of pages to be scanned, including how to scan them, and how to describe them
 */
export interface ISuite {
  match?: string;
  pages?: IPage[];
}
/**
 * @typedef IFile
 * @property {string} match - The path, relative to project root, of file that, when matched, causes suite to be added
 * @property {IPage[]} pages - List of pages to be scanned, including how to scan them, and how to describe them
 */
export interface IFile extends ISuite {
  match: string;
  pages?: IPage[];
}
/**
 * @typedef IExpression
 * @property {string} match - Regular expression literal, converted to JSON-safe string, used to make suite, if matched
 * @property {boolean} once - Determines if, once matched, the expression is removed from list of expressions to match
 * @property {IPage[]} pages - List of pages to be scanned, including how to scan them, and how to describe them
 */
export interface IExpression extends ISuite {
  match: string;
  once?: boolean;
  pages?: IPage[];
}

/**
 * @typedef IFullConf
 * @property {IFile[]} files - Specifies files that when matched cause suites of tests to be run
 * @property {IExpression[]} once - Specifies regular expressions that when matched cause suites of tests to be run
 * @property {ITemplate[]} templates - Templates contain conf objects that can be merged with templates, including pages
 */
export interface IFullConf {
  files?: IFile[];
  expressions?: IExpression[];
  templates?: ITemplate[];
  // [key: string]: any;
}

/**
 * @typedef IParseConf
 * @property {IFullConf[]} fullConf - Contains full configuration data to be parsed
 * @property {ISuite[]} fullSuite - The full set of test suites to be run based on info in fullConf and list of paths
 */
export interface IParseConf {
  fullConf: IFullConf;
  readonly fullSuite: ISuite[];
}
