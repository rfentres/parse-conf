import * as chai from "chai";
import * as axe from "axe-core";
import * as _ from "lodash";
import { Scan } from "../../src/ParseConf/Scan";
const expect = chai.expect;

describe('Scan', function() {

  describe('#new(confObject)', function () {
    describe('constructor', function () {
      it('should not throw an error when conf object is passed', function () {
        expect(function () {
          new Scan({
            "meta": {
              "title": "Main post",
              "description": "Tests all rules."
            },
            "context": {
              "include": [["body"]],
              "exclude": [[".tota11y-toolbar"]]
            },
            "options": {
              "runOnly": {
                "type": "tag",
                "values": ["wcag2a", "wcag2aa", "best-practice"]
              }
            }
          })
        }).to.not.throw();
      });
    });
  });


  describe('value of title', function () {

    describe('when conf.scan.title provided in constructor', function () {
      it('should be value of conf.scan.title, even when conf.context provided', function () {
        let scan = new Scan({
          "meta": {
            "title": "Main post",
            "description": "Tests all rules."
          },
          "context": {
            "include": [["body"]],
            "exclude": [[".tota11y-toolbar"]]
          }
        });
        expect(scan.title).to.equal("Main post");
      });
    });

    describe('when conf.meta is NOT provided in constructor', function () {
      describe('when both context.include & context.exclude provided', function () {
        it('should be "+[...(context.include)], -[...(context.exclude)]"', function () {
          let scan = new Scan({
            "context": {
              "include": [["body"]],
              "exclude": [[".tota11y-toolbar"]]
            }
          });
          expect(scan.title).to.equal('+[["body"]], -[[".tota11y-toolbar"]]');
        });
      });
      describe('when only context.include provided', function () {
        it('should be "+[...(context.include)]"', function () {
          let scan = new Scan({
            "context": {
              "include": [["body"]]
            }
          });
          expect(scan.title).to.equal('+[["body"]]');
        });
      });
      describe('when only context.exclude provided', function () {
        it('should be "+document, -[...(context.exclude)]"', function () {
          let scan = new Scan({
            "context": {
              "exclude": [[".tota11y-toolbar"]]
            }
          });
          expect(scan.title).to.equal('+document, -[[".tota11y-toolbar"]]');
        });
      });
      describe('when neither context.include nor context.exclude provided', function () {
        it('should be "+document"', function () {
          let scan = new Scan({
            "context": {}
          });
          expect(scan.title).to.equal('+document');
        });
      });
      describe('when no context provided at all', function () {
        it('should be "+document"', function () {
          let scan = new Scan({});
          expect(scan.title).to.equal('+document');
        });
      });
    });
  });

  describe('value of description', function () {

    let noDesc: string = 'No rules or tags specified';

    describe('when conf.scan.description provided in constructor', function () {
      it('should be value of conf.scan.description, even when conf.options provided', function () {
        let scan = new Scan({
          "meta": {
            "title": "Main post",
            "description": "Tests all rules."
          },
          "options": {
            "runOnly": {
              "type": "tag",
              "values": ["wcag2a", "wcag2aa"]
            }
          }
        });
        expect(scan.description).to.equal("Tests all rules.");
      });
    });

    describe('when conf.meta is NOT provided in constructor', function () {
      describe('when options is set to EMPTY object', function () {
        it('should indicate that no rules or tags have been specified', function () {
          let scan = new Scan({
            "options": {}
          });
          expect(scan.description).to.equal(noDesc);
        });
      });
      describe('when options.rules has NOT been set', function () {
        describe('when options.runOnly.type is tag', function () {
          it('should be "+tags[...(options.runOnly.values)]"', function () {
            let scan = new Scan({
              "options": {
                "runOnly": {
                  "type": "tag",
                  "values": ["wcag2a", "wcag2aa"]
                }
              }
            });
            expect(scan.description).to.equal('+tags["wcag2a","wcag2aa"]');
          });
        });
        describe('when options.runOnly.type is rule', function () {
          describe('when options.runOnly.values has been set', function () {
            it('should be "+rules[...(options.runOnly.values)]', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "rule",
                    "values": ["button-name", "color-contrast"]
                  }
                }
              });
              expect(scan.description).to.equal('+rules["button-name","color-contrast"]');
            });
          });
          describe('when options.runOnly.values has NOT been set', function () {
            it('should indicate that no rules or tags have been specified', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "rule"
                  }
                }
              });
              expect(scan.description).to.equal(noDesc);
            });
          });
        });
        describe('when options.runOnly.type is tags', function () {
          describe('when runOnly.value is NOT set', function () {
            it('should indicate that no rules or tags have been specified', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "tags"
                  }
                }
              });
              expect(scan.description).to.equal(noDesc);
            });
          });
          describe('when runOnly.value is set to EMPTY object', function () {
            it('should indicate that no rules or tags have been specified', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "tags",
                    "value": {}
                  }
                }
              });
              expect(scan.description).to.equal(noDesc);
            });
          });
          describe('when runOnly.value.include is NOT set', function () {
            describe('when runOnly.value.exclude is NOT set', function () {
              it('should indicate that no rules or tags have been specified', function () {
                let scan = new Scan({
                  "options": {
                    "runOnly": {
                      "type": "tags",
                      "value": {}
                    }
                  }
                });
                expect(scan.description).to.equal(noDesc);
              });
            });
          });
          describe('when runOnly.value.include is set', function () {
            describe('when runOnly.value.exclude is set', function () {
              it('should be "+tags[...(options.runOnly.value.include)], -tags[...(options.runOnly.value.exclude)]"', function () {
                let scan = new Scan({
                  "options": {
                    "runOnly": {
                      "type": "tags",
                      "value": {
                        "include": ["wcag2a", "wcag2aa"],
                        "exclude": ["experimental"]
                      }
                    }
                  }
                });
                expect(scan.description).to.equal('+tags["wcag2a","wcag2aa"], -tags["experimental"]');
              });
            });
            describe('when runOnly.value.exclude is NOT set', function () {
              it('should be "+tags[...(options.runOnly.value.include)]"', function () {
                let scan = new Scan({
                  "options": {
                    "runOnly": {
                      "type": "tags",
                      "value": {
                        "include": ["wcag2a", "wcag2aa"]
                      }
                    }
                  }
                });
                expect(scan.description).to.equal('+tags["wcag2a","wcag2aa"]');
              });
            });
            describe('when runOnly.value.exclude is set to EMPTY array', function () {
              it('should be "+tags[...(options.runOnly.value.include)]"', function () {
                let scan = new Scan({
                  "options": {
                    "runOnly": {
                      "type": "tags",
                      "value": {
                        "include": ["wcag2a", "wcag2aa"],
                        "exclude": []
                      }
                    }
                  }
                });
                expect(scan.description).to.equal('+tags["wcag2a","wcag2aa"]');
              });
            });
          });
        });
      });
      describe('when options.rules has been set', function () {
        describe('when options.runOnly has NOT been set', function () {
          describe('when options.rules is set to non-empty object', function () {
            it('should be "+rules[keys in options.rules]"', function () {
              let scan = new Scan({
                "options": {
                  "rules": {
                    "color-contrast": {enabled: true},
                    "valid-lang": {enabled: true}
                  }
                }
              });
              expect(scan.description).to.equal('+rules["valid-lang","color-contrast"]');
            });
          });
          describe('when options.rules is set to EMPTY object', function () {
            it('should indicate that no rules or tags have been specified', function () {
              let scan = new Scan({
                "options": {
                  "rules": {}
                }
              });
              expect(scan.description).to.equal(noDesc);
            });
          });
        });
        describe('when options.runOnly.type is tag', function () {
          describe('when options.rules[key].enabled can be true or false', function () {
            it('should be "+tags[...(options.runOnly.values)], +rules[...(keys in options.rules where enabled is true)], -rules[...(keys in options.rules where enabled is false)]"', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "tag",
                    "values": ["wcag2a"]
                  },
                  "rules": {
                    "color-contrast": {"enabled": true},
                    "valid-lang": {"enabled": false}
                  }
                }
              });
              expect(scan.description).to.equal('+tags["wcag2a"], +rules["color-contrast"], -rules["valid-lang"]');
            });
          });
        });
        describe('when options.runOnly.type is rule', function () {
          describe('when options.rules[key].enabled is always true', function () {
            it('should be "+rules[...(keys in options.rules), ...(options.runOnly.values)]"', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "rule",
                    "values": ["button-name", "link-in-text-block"]
                  },
                  "rules": {
                    "color-contrast": {enabled: true},
                    "valid-lang": {enabled: true}
                  }
                }
              });
              expect(scan.description).to.equal('+rules["valid-lang","color-contrast","button-name","link-in-text-block"]');
            });
          });
          describe('when options.rules[key].enabled is always false', function () {
            it('should be "+rules[...(options.runOnly.values)], -rules[...(keys in options.rules)]"', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "rule",
                    "values": ["button-name", "link-in-text-block"]
                  },
                  "rules": {
                    "button-name": {enabled: false},
                    "link-in-text-block": {enabled: false}
                  }
                }
              });
              expect(scan.description).to.equal('+rules["button-name","link-in-text-block"], -rules["link-in-text-block","button-name"]');
            });
          });
          describe('when options.rules[key].enabled can be true or false', function () {
            it('should be "+rules[...(options.runOnly.values), ...(keys in options.rules where enabled is true)], -rules[...(keys in options.rules where enabled is false)]"', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "rule",
                    "values": ["button-name", "link-in-text-block"]
                  },
                  "rules": {
                    "button-name": {enabled: false},
                    "valid-lang": {enabled: true}
                  }
                }
              });
              expect(scan.description).to.equal('+rules["valid-lang","button-name","link-in-text-block"], -rules["button-name"]');
            });
          });
        });
        describe('when options.runOnly.type is tags', function () {
          describe('when options.runOnly.type.value is set to an EMPTY object', function () {
            it('should indicate that no rules or tags have been specified', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "tags",
                    "value": {}
                  }
                }
              });
              expect(scan.description).to.equal(noDesc);
            });
          });
          describe('when options.runOnly.value.include is set', function () {
            describe('when options.runOnly.value.exclude is set', function () {
              describe('when options.rules contains both enabled and disabled rules', function () {
                it('should be "+tags[...(options.runOnly.value.include)], -tags[...(options.runOnly.value.exclude)], +rules[...(options.runOnly.values), ...(keys in options.rules where enabled is true)], -rules[...(keys in options.rules where enabled is false)]"', function () {
                  let scan = new Scan({
                    "options": {
                      "runOnly": {
                        "type": "tags",
                        "value": {
                          "include": ["wcag2a"],
                          "exclude": ["experimental"]
                        }
                      },
                      "rules": {
                        "button-name": {enabled: false},
                        "valid-lang": {enabled: true}
                      }
                    }
                  });
                  expect(scan.description).to
                    .equal('+tags["wcag2a"], -tags["experimental"], +rules["valid-lang"], -rules["button-name"]');
                });
              });
            });
          });
        });
      });
    });
  });

  describe('value of rules', function () {
    let rules = {
      configured: _.map(axe.getRules(), "ruleId"),
      /* [
       'accesskeys',
       'area-alt',
       'aria-allowed-attr',
       'aria-required-attr',
       'aria-required-children',
       'aria-required-parent',
       'aria-roles',
       'aria-valid-attr-value',
       'aria-valid-attr',
       'audio-caption',
       'blink',
       'button-name',
       'bypass',
       'checkboxgroup',
       'color-contrast',
       'definition-list',
       'dlitem',
       'document-title',
       'duplicate-id',
       'empty-heading',
       'frame-title',
       'heading-order',
       'href-no-hash',
       'html-has-lang',
       'html-lang-valid',
       'image-alt',
       'image-redundant-alt',
       'input-image-alt',
       'label-title-only',
       'label',
       'layout-table',
       'link-in-text-block',
       'link-name',
       'list',
       'listitem',
       'marquee',
       'meta-refresh',
       'meta-viewport-large',
       'meta-viewport',
       'object-alt',
       'radiogroup',
       'region',
       'scope-attr-valid',
       'server-side-image-map',
       'skip-link',
       'tabindex',
       'table-duplicate-name',
       'table-fake-caption',
       'td-has-header',
       'td-headers-attr',
       'th-has-data-cells',
       'valid-lang',
       'video-caption',
       'video-description'] */
      experimental: _.map(axe.getRules(["experimental"]), "ruleId"),
      /* ['link-in-text-block', 'table-fake-caption', 'td-has-header'] */
      wcag2a: _.map(axe.getRules(["wcag2a"]), "ruleId"),
      /* ['accesskeys',
        'area-alt',
        'aria-allowed-attr',
        'aria-required-attr',
        'aria-required-children',
        'aria-required-parent',
        'aria-roles',
        'aria-valid-attr-value',
        'aria-valid-attr',
        'audio-caption',
        'blink',
        'button-name',
        'bypass',
        'definition-list',
        'dlitem',
        'document-title',
        'duplicate-id',
        'frame-title',
        'html-has-lang',
        'html-lang-valid',
        'image-alt',
        'input-image-alt',
        'label',
        'layout-table',
        'link-in-text-block',
        'link-name',
        'list',
        'listitem',
        'marquee',
        'meta-refresh',
        'object-alt',
        'server-side-image-map',
        'table-fake-caption',
        'td-has-header',
        'td-headers-attr',
        'th-has-data-cells',
        'video-caption'] */
      wcag2aa: _.map(axe.getRules(["wcag2aa"]), "ruleId"),
      /* ['color-contrast', 'meta-viewport', 'valid-lang', 'video-description'] */
      wcag2aaa: _.map(axe.getRules(["wcag2aaa"]), "ruleId"),
      /* ['meta-refresh'] */
      section508: _.map(axe.getRules(["section508"]), "ruleId"),
      /* ['area-alt',
        'audio-caption',
        'blink',
        'button-name',
        'bypass',
        'frame-title',
        'image-alt',
        'input-image-alt',
        'label',
        'link-name',
        'object-alt',
        'server-side-image-map',
        'table-fake-caption',
        'td-has-header',
        'td-headers-attr',
        'th-has-data-cells',
        'video-caption',
        'video-description'] */
    };

    describe('when options is set to an EMPTY object', function () {
      it('should be all rules in the default configuration', function () {
        let scan = new Scan({
          "options": {}
        });
        expect(scan.rules).to.eql(rules.configured);
      });
    });

    describe('when options.runOnly is NOT set', function () {
      describe('when options.rules is an EMPTY object', function () {
        it('should be all rules in the default configuration', function () {
          // Adding rules is irrelevant, since the default is all rules
          let scan = new Scan({
            "options": {
              "rules": {}
            }
          });
          expect(scan.rules).to.eql(rules.configured);
        });
      });
      describe('when options.rules is set to non-empty object', function () {
          describe('when all rules in options.rules have their enabled properties set to true', function () {
            it('should be all rules in the default configuration', function () {
              // Adding rules is irrelevant, since the default is all rules
              let scan = new Scan({
                "options": {
                  "rules": {
                    "color-contrast": {enabled: true},
                    "valid-lang": {enabled: true}
                  }
                }
              });
              expect(scan.rules).to.have.members(rules.configured);
              expect(scan.rules).to.have.lengthOf(rules.configured.length);
            });
          });
          describe('when any rules in options.rules have their enabled properties set to false', function () {
            it('should be all rules in the default configuration, minus those set to false', function () {

              let diff = _.difference(rules.configured, ["color-contrast", "video-caption"]);

              // If NO other rules set to true
              let withoutEnabled = new Scan({
                "options": {
                  "rules": {
                    "color-contrast": {enabled: false},
                    "video-caption": {enabled: false}
                  }
                }
              });
              expect(withoutEnabled.rules).to.have.members(diff);
              expect(withoutEnabled.rules).to.have.lengthOf(diff.length);

              // If no other rules ARE set to true
              let withEnabled = new Scan({
                "options": {
                  "rules": {
                    "color-contrast": {enabled: false},
                    "video-caption": {enabled: false},
                    "valid-lang": {enabled: true}
                  }
                }
              });
              expect(withEnabled.rules).to.have.members(diff);
              expect(withEnabled.rules).to.have.lengthOf(diff.length);

            });
          });
      });
    });

    describe('when options.rules is NOT set', function () {
      describe('when options.runOnly is set', function () {
        describe('when options.runOnly.type is NOT set', function () {
          it('should be all rules in default configuration', function () {
            let scan = new Scan({
              "options": {}
            });
            expect(scan.rules).to.eql(rules.configured);
          });
        });
        describe('when options.runOnly.type is tag', function () {
          describe('when options.runOnly.values has NOT been set', function () {
            it('should be empty array', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "tag"
                  }
                }
              });
              expect(scan.rules).to.be.empty;
            });
          });
          describe('when options.runOnly.values is set to EMPTY array', function () {
            it('should be empty array', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "tag",
                    "values": []
                  }
                }
              });
              expect(scan.rules).to.be.empty;
            });
          });
          describe('when options.runOnly.values.length = 1', function () {
            it('should be all rules with the tag specified in options.runOnly.values', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "tag",
                    "values": ["wcag2aa"]
                  }
                }
              });
              // console.log(scan.rules);
              expect(scan.rules).to.eql(rules.wcag2aa);
            });
          });
          describe('when options.runOnly.values.length = 2', function () {
            describe('when no rules have tags in common', function () {
              it('should be combined set of rules', function () {
                let scan = new Scan({
                  "options": {
                    "runOnly": {
                      "type": "tag",
                      "values": ["wcag2aa", "experimental"]
                    }
                  }
                });
                let combinedSets = _.union(rules.wcag2aa, rules.experimental);
                expect(scan.rules).to.have.members(combinedSets);
                expect(scan.rules).to.have.lengthOf(combinedSets.length);
              });
            });
            describe('when rules have tags in common', function () {
              describe('when first set of rules is smaller than the second', function () {
                it('should be union of sets of rules', function () {
                  let scan = new Scan({
                    "options": {
                      "runOnly": {
                        "type": "tag",
                        "values": ["wcag2aa", "section508"]
                      }
                    }
                  });
                  let unionOfSets = _.union(rules.wcag2aa, rules.section508);
                  /* unionOfSets should be ['color-contrast', 'meta-viewport', 'valid-lang', 'video-description',
                    'area-alt', 'audio-caption', 'blink', 'button-name', 'bypass', 'frame-title', 'image-alt',
                    'input-image-alt', 'label', 'link-name', 'object-alt', 'server-side-image-map', 'table-fake-caption',
                    'td-has-header', 'td-headers-attr', 'th-has-data-cells', 'video-caption'] */
                  expect(scan.rules).to.have.members(unionOfSets);
                  expect(scan.rules).to.have.lengthOf(unionOfSets.length);
                });
              });
              describe('when second set of rules is smaller than the first', function () {
                it('should be union of sets of rules', function () {
                  let scan = new Scan({
                    "options": {
                      "runOnly": {
                        "type": "tag",
                        "values": ["section508", "wcag2aa"]
                      }
                    }
                  });
                  let unionOfSets = ['area-alt', 'audio-caption', 'blink', 'button-name', 'bypass', 'frame-title',
                    'image-alt', 'input-image-alt', 'label', 'link-name', 'object-alt', 'server-side-image-map',
                    'table-fake-caption', 'td-has-header', 'td-headers-attr', 'th-has-data-cells', 'video-caption',
                    'color-contrast', 'meta-viewport', 'valid-lang', 'video-description'];
                  expect(scan.rules).to.have.members(unionOfSets);
                  expect(scan.rules).to.have.lengthOf(unionOfSets.length);
                });
              });
            });
          });
          describe('when options.runOnly.values.length = 3', function () {
            describe('when no rules have tags in common', function () {
              it('should be combined set of rules', function () {
                let scan = new Scan({
                  "options": {
                    "runOnly": {
                      "type": "tag",
                      "values": ["wcag2aa", "wcag2aaa", "experimental"]
                    }
                  }
                });
                let combinedSets = _.union(rules.wcag2aa, rules.wcag2aaa, rules.experimental);
                expect(scan.rules).to.have.members(combinedSets);
                expect(scan.rules).to.have.lengthOf(combinedSets.length);
              });
            });
            describe('when tags have rules in common', function () {
              let unionOfSets = [

                /* wcag2a */
                'accesskeys',
                'area-alt',
                'aria-allowed-attr',
                'aria-required-attr',
                'aria-required-children',
                'aria-required-parent',
                'aria-roles',
                'aria-valid-attr-value',
                'aria-valid-attr',
                'audio-caption',
                'blink',
                'button-name',
                'bypass',
                'definition-list',
                'dlitem',
                'document-title',
                'duplicate-id',
                'frame-title',
                'html-has-lang',
                'html-lang-valid',
                'image-alt',
                'input-image-alt',
                'label',
                'layout-table',
                'link-in-text-block',
                'link-name',
                'list',
                'listitem',
                'marquee',
                'meta-refresh',
                'object-alt',
                'server-side-image-map',
                'table-fake-caption',
                'td-has-header',
                'td-headers-attr',
                'th-has-data-cells',
                'video-caption',

                /* wcag2aa */
                'color-contrast', 'meta-viewport', 'valid-lang', 'video-description',

                /* section508 */
                /*'area-alt',
                 'audio-caption',
                 'blink',
                 'button-name',
                 'bypass',
                 'frame-title',
                 'image-alt',
                 'input-image-alt',
                 'label',
                 'link-name',
                 'object-alt',
                 'server-side-image-map',
                 'table-fake-caption',
                 'td-has-header',
                 'td-headers-attr',
                 'th-has-data-cells',
                 'video-caption',
                 'video-description'*/

              ];
              describe('when size order is largest, smallest, middle', function () {
                it('should be union of sets of rules', function () {
                  let scan = new Scan({
                    "options": {
                      "runOnly": {
                        "type": "tag",
                        "values": ["wcag2a", "wcag2aa", "section508"]
                      }
                    }
                  });
                  expect(scan.rules).to.have.members(unionOfSets);
                  expect(scan.rules).to.have.lengthOf(unionOfSets.length);
                });
              });
              describe('when size order is smallest, largest, middle', function () {
                it('should be union of sets of rules', function () {
                  let scan = new Scan({
                    "options": {
                      "runOnly": {
                        "type": "tag",
                        "values": ["wcag2aa", "wcag2a", "section508"]
                      }
                    }
                  });
                  expect(scan.rules).to.have.members(unionOfSets);
                  expect(scan.rules).to.have.lengthOf(unionOfSets.length);
                });
              });
              describe('when size order is middle, smallest, largest', function () {
                it('should be union of sets of rules', function () {
                  let scan = new Scan({
                    "options": {
                      "runOnly": {
                        "type": "tag",
                        "values": ["section508", "wcag2aa", "wcag2a",]
                      }
                    }
                  });
                  expect(scan.rules).to.have.members(unionOfSets);
                  expect(scan.rules).to.have.lengthOf(unionOfSets.length);
                });
              });
              describe('when size order is middle, largest, smallest', function () {
                it('should be union of sets of rules', function () {
                  let scan = new Scan({
                    "options": {
                      "runOnly": {
                        "type": "tag",
                        "values": ["section508", "wcag2a", "wcag2aa",]
                      }
                    }
                  });
                  expect(scan.rules).to.have.members(unionOfSets);
                  expect(scan.rules).to.have.lengthOf(unionOfSets.length);
                });
              });
            });
          });
        });
        describe('when options.runOnly.type is rule', function () {
          describe('when options.runOnly.values has been set', function () {
            it('should be "+rules[values]', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "rule",
                    "values": ["button-name", "color-contrast"]
                  }
                }
              });
              expect(scan.rules).to.eql(["button-name", "color-contrast"]);
            });
          });
          describe('when options.runOnly.values has NOT been set', function () {
            it('should be empty array', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "rule"
                  }
                }
              });
              expect(scan.rules).to.be.empty;
            });
          });
        });
        describe('when options.runOnly.type is tags', function () {
          describe('when runOnly.value is NOT set', function () {
            it('should be empty array', function () {
              let scan = new Scan({
                "options": {
                  "runOnly": {
                    "type": "tags"
                  }
                }
              });
              expect(scan.rules).to.be.empty;
            });
          });
          describe('when runOnly.value.include is NOT set', function () {
            describe('when runOnly.value.exclude is NOT set', function () {
              it('should be empty array', function () {
                let scan = new Scan({
                  "options": {
                    "runOnly": {
                      "type": "tags",
                      "value": {}
                    }
                  }
                });
                expect(scan.rules).to.be.empty;
              });
            });
          });
          describe('when runOnly.value.include is set', function () {
            describe('when runOnly.value.include.length > 0', function () {
              describe('when runOnly.value.exclude is set', function () {
                it('should be the union of all rules with tags in runOnly.value.include, minus the union of all rules with tags in runOnly.value.exclude', function () {
                  let scan = new Scan({
                    "options": {
                      "runOnly": {
                        "type": "tags",
                        "value": {
                          "include": ["wcag2a", "wcag2aa"],
                          "exclude": ["experimental", "wcag2aaa"]
                        }
                      }
                    }
                  });
                  let incRules = <string[]> _.union(rules.wcag2a, rules.wcag2aa);
                  let excRules = <string[]> _.union(rules.experimental, rules.wcag2aaa);
                  let diff = _.difference(incRules, excRules);
                  expect(scan.rules).to.have.members(diff);
                  expect(scan.rules).to.have.lengthOf(diff.length);
                });
              });
              describe('when runOnly.value.exclude is NOT set', function () {
                it('should be the union of all rules with tags in runOnly.value.include', function () {
                  let scan = new Scan({
                    "options": {
                      "runOnly": {
                        "type": "tags",
                        "value": {
                          "include": ["wcag2a", "wcag2aa"]
                        }
                      }
                    }
                  });
                  let incRules = _.union(rules.wcag2a, rules.wcag2aa);
                  expect(scan.rules).to.have.members(incRules);
                  expect(scan.rules).to.have.lengthOf(incRules.length);
                });
              });
              describe('when runOnly.value.exclude is set to EMPTY array', function () {
                it('should be the union of all rules with tags in runOnly.value.include', function () {
                  let scan = new Scan({
                    "options": {
                      "runOnly": {
                        "type": "tags",
                        "value": {
                          "include": ["wcag2a", "wcag2aa"],
                          "exclude": []
                        }
                      }
                    }
                  });
                  let incRules = _.union(rules.wcag2a, rules.wcag2aa);
                  expect(scan.rules).to.have.members(incRules);
                  expect(scan.rules).to.have.lengthOf(incRules.length);
                });
              });
            });
            describe('when runOnly.value.include is set to EMPTY array', function () {
              it('should be empty array', function () {

                let noExclude = new Scan({
                  "options": {
                    "runOnly": {
                      "type": "tags",
                      "value": {
                        "include": []
                      }
                    }
                  }
                });
                expect(noExclude.rules).to.be.empty;

                let emptyExclude = new Scan({
                  "options": {
                    "runOnly": {
                      "type": "tags",
                      "value": {
                        "include": [],
                        "exclude": []
                      }
                    }
                  }
                });
                expect(emptyExclude.rules).to.be.empty;

                let nonEmptyExclude = new Scan({
                  "options": {
                    "runOnly": {
                      "type": "tags",
                      "value": {
                        "include": [],
                        "exclude": ["experimental", "wcag2aaa"]
                      }
                    }
                  }
                });
                expect(nonEmptyExclude.rules).to.be.empty;
              });
            });
          });
        });
      })
    });

    /**
     * When rules is set it only adds or removes from the defaults set of rules or the rules set in runOnly.
     * Since we've tested what happens when options.runOnly or options.rules are run exclusively and that works,
     * we'll just try one test condition with both enabled and assume the rest.
     */
    describe('when options.runOnly is set', function () {
      describe('when options.rules is set', function () {
        it('should be all rules from runOnly, + the enabled ones in rules, - the disabled ones in rules', function () {
          // wcag2aa is ['color-contrast', 'meta-viewport', 'valid-lang', 'video-description']
          let scan = new Scan({
            "options": {
              "runOnly": {
                "type": "tag",
                "values": ["wcag2aa"]
              },
              "rules": {
                "color-contrast": {enabled: false},
                "td-has-header": {enabled: true}
              }
            }
          });
          let union = <string[]> _.union(rules.wcag2aa, ["td-has-header"]);
          let diff = _.difference(union, ["color-contrast"]);
          // diff should be ['td-has-header', 'meta-viewport', 'valid-lang', 'video-description']
          expect(scan.rules).to.have.members(diff);
          expect(scan.rules).to.have.lengthOf(diff.length);
        });
      });
    });

  });
});
