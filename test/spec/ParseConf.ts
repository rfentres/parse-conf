import * as chai from "chai";
import { Parse } from "../../src/ParseConf";
const expect = chai.expect;
const fullConf = require("./../full.json");

describe('Parse', function() {
  const confPath: string = "./../../test/full.json";
  const files: string[] = [
    "templates/1.php",
    "templates/2.php",
    "templates/3.php",
    "assets/scripts/1.js",
    "assets/scripts/2.js",
    "assets/styles/1.scss",
    "assets/styles/2.scss"
  ];
  describe('#new('+JSON.stringify(files)+', "'+fullConf+'")', function() {
    describe('constructor', function() {
      it('should not throw an error when templates object is passed', function() {
        expect(function() { new Parse(fullConf) }).to.not.throw();
      });
    });
  });
  describe('default values after constructor', function() {
    it('should return the required conf file as the value of fullConf', function() {
      let full:Parse = new Parse(fullConf);
      expect(full.fullConf).to.eql(fullConf);
    });
  });
  describe('#expand', function () {
    describe('#expandTemplates(false)', function () {
      it('should NOT replace options with version that has converted everything to runOnly.type = rules', function () {
        let full:Parse = new Parse(fullConf);
        full.createSuites(files.slice());
        full.expandTemplates(false);
        // console.log(JSON.stringify(full.fullSuite[2].pages[0], null, "  "));
        let options = full.fullSuite[2].pages[0].conf.options;
        expect(options).to.eql({
          "runOnly": {
            "type": "tag",
            "values": ["wcag2aa"]
          }
        });
      });
    });
    describe('#expandTemplates()', function () {
      let parser:Parse;
      before(function (done) {
        parser = new Parse(fullConf);
        // console.log(JSON.stringify(parser.fullConf, null, "  "));
        parser.createSuites(files.slice());
        parser.expandTemplates();
        done();
      });

      it('should add title if none set in match, but is set in template hierarchy', function () {
        expect(parser.fullSuite[0].pages[0].conf.meta.title).to.equal("Main post");
      });

      it('should not overwrite context, but, should, instead, merge with parents in template hierarchy', function () {
        let context = parser.fullSuite[0].pages[0].conf.context;
        expect(context.include).to.eql([[".main"]]);
        expect(context.exclude).to.eql([[".tota11y-toolbar"]]);
      });

      it('should overwrite exclude within context, though', function () {
        let context = parser.fullSuite[1].pages[0].conf.context;
        expect(context.include).to.eql([[".main"]]);
        expect(context.exclude).to.eql([[".excluded"]]);
      });

      it('should create title if meta object not passed in constructor', function () {
        let meta = parser.fullSuite[2].pages[0].conf.meta;
        expect(meta.title).to.eql('+[["body"]], -[[".tota11y-toolbar"]]');
      });

      it('should create description if meta object not passed in constructor', function () {
        let meta = parser.fullSuite[2].pages[0].conf.meta;
        expect(meta.description).to.eql('+tags["wcag2aa"]');
      });

      it('should replace options', function () {
        let options = parser.fullSuite[2].pages[0].conf.options;
        expect(options).to.eql({
          "runOnly": {
            "type": "rule",
            "values": ['color-contrast', 'meta-viewport', 'valid-lang', 'video-description']
          }
        });
      });
    });
  });
  describe('#createSuites()', function() {
    let full:Parse;
    before(function (done) {
      full = new Parse(fullConf);
      full.createSuites(files.slice());
      done();
    });

    it('should have removed the matched files from conf', function () {
      expect(full.tempConf.files).to.not.include({
        "match": "templates/1.php",
        "pages": [
          {
            "match": "/",
            "template": "mergeContent"
          }
        ]
      });
      expect(full.tempConf.files).to.not.include({
        "match": "templates/2.php",
        "pages": [
          {
            "match": "/",
            "template": "overrideExclude"
          }
        ]
      });
    });

    it('should have removed the matched expressions set to only run once from conf', function () {
      expect(full.tempConf.expressions).to.not.include({
        "match": "/^(assets\\/scripts\\/).*\\.js$/i",
        "once": true,
        "pages": [
          {
            "match": "/",
            "template": "js"
          }
        ]
      });
    });

    it('should not add to fullSuite if path matches run-once expression, but has been matched previously', function () {
      expect(full.fullSuite).to.not.include({
        "match": "assets/scripts/2.js",
        "pages": [
          {
            "match": "/",
            "template": "js"
          }
        ]
      });
    });
  });
  describe('#getPageSuite("/")', function() {
    let full:Parse;
    let suite;
    before(function (done) {
      full = new Parse(fullConf);
      suite = full.getPageSuite("/");
      //console.log(JSON.stringify(suite, null, "  "));
      done();
    });

    describe('when no matches in pages', function() {
      /*it('should have not be added to fullSuite', function () {
        expect(full.fullSuite).to.not.include({
          "match": "templates/2.php",
          "template": "overrideExclude"
        });
      });*/
      it('should not be removed from tempConf', function () {
        expect(full.tempConf.files).to.not.include({
          "match": "/nomatch",
          "template": "overrideExclude"
        });
      });
    });
/*    describe('when there is only one page in suite and it matches', function() {
      it('should be added to fullSuite', function () {
        expect(full.fullSuite).to.include({
          "match": "templates/1.php",
          "template": "mergeContext"
        });
      });
    });
    describe('when there are multiple pages in a suite and one matches', function() {
      it('should be added to fullSuite', function () {
        expect(full.fullSuite).to.include({
          "match": "templates/3.php",
          "template": "noPlugin"
        });
      });
    });*/
  });
});
