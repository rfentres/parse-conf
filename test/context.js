let Context = require("../build/ParseConf").Context;

describe('Context', function() {
    describe('#new({title:"title", description:"description"})', function() {
        describe('constructor', function() {
            it('should not throw an error when title and description are passed', function () {
                expect(function() { new Context({title:"title", description:"description"}) }).to.not.throw();
            });
        });

        describe('default values after constructor', function() {
            let context = new Context({title:"title", description:"description"});

            it('should return the value of title set in the constructor', function() {
                expect(context.title).to.equal('title');
            });

            it('should return the value of description set in the constructor', function() {
                expect(context.description()).to.equal("description");
            });
        })
    })
});
