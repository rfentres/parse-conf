/*
import { Greeter } from "../src/greeter";
import * as axe from "axe-core";
import * as chai from "chai";
let fullConf = require("./full.json");
import { Templates } from "../src/ParseConf";
//let TemplateService = ParseConf.TemplateService;

const expect = chai.expect;

//let context:any = document;

describe("greeter", () => {
  it("should greet with message", () => {
    const greeter = new Greeter("friend");
    expect(greeter.greet()).to.equal("Bonjour, friend!");
  });
});

describe("Templates", () => {
  describe('#new(fullConf.templates)', function() {
    describe('constructor', function() {
      it('should not throw an error when templates object is passed', function () {
        expect(function() { new Templates(fullConf.templates) }).to.not.throw();
      });
    });

    describe('default values after constructor', function() {
      let templateService = new Templates(fullConf.templates);

      it('should return the match property of first template object in templates set in the constructor', function() {
        expect(templateService.templates[0].match).to.equal('baser');
      });
    })
  })
});
/!*function getRuleId(rule:{ruleId:String}) {
  console.log(rule.ruleId);
}
axe.getRules().map(getRuleId);*!/
*/
